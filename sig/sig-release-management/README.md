# sig-release-management
- key requirement and feature management
- release plan, development plan, branch plan
- development process and risk management
- release and release notes

# meeting
- to discuss and decide

# member

### Maintainer
- zyp[@zyp-rock](https://gitee.com/zyp-rock)
- yumin_jiang[@yumin_jiang](https://gitee.com/yumin_jiang)

### Committer
- zyp[@zyp-rcok](https://gitee.com/zyp-rock)
- yumin_jiang[@yumin_jiang](https://gitee.com/yumin_jiang)
- Ronnie_Jiang[@Ronnie_Jiang](https://gitee.com/Ronnie_Jiang)
- ChenYaqiang[@yaqiangchen](https://gitee.com/yaqiangchen)
- solarhu[@solarhu](https://gitee.com/solarhu)

# communication
- maillist: <dev@openeuler.org>

# repository
- openeuler/release-management
