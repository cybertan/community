## 概述

Java SIG 致力于将 Java 生态中大量高质量开源应用、组件引入 openEuler 社区和系统，通过提供完善的构建解决方案、环境和指引文档，让更多对此感兴趣的人加入其中。

## 目标

- 首先在社区引入 `maven`,`ant`,`gradle`,`sbt` 这样的构建工具，为其他 Java 应用的引入做好准备。当然这里主要是 `maven`。
- 定义编写 maven 构建相关的宏和脚本，方便其他需要使用 maven 构建的 spec 的编写。
- 按需引入 Java 生态中的重要软件包，例如 tomcat、jetty、eclipse 等。
- 编写针对 Java 软件包的 spec 的指引文档，让更多人可以参与进来。
- 创建一些针对不同 JDK 版本的 docker 镜像，这些镜像集成了完整的构建工具链（包括 maven 内部仓储）和环境，方便不同用途的构建和验证工作，例如开发者实验环境、CI 系统等。

## 任务

- 引入 maven：通过 rpm 的方式引入 maven 到社区。
- 定义 maven spec 宏：定义一系列用于简化针对 maven 构建的宏定义。
- Java 应用构建指引文档：编写构建和宏使用相关的说明文档。
- 引入更多重要 Java 应用和组件：例如 tomcat, jetty 等。
- 开放应用引入清单：鼓励更多人参与到 Java 应用引入的行列来。

`<figure>`

## 组织会议

- 公开的会议时间：
- Zoom MeetID: 

## 成员

### Maintainer 列表

- 雒海波[@luo-haibo](https://gitee.com/luo-haibo)

### Committer 列表
- 

## 联系方式

- 邮件列表: 
- Slack 群组: 
- IRC 频道:
- Zoom 公开会议 MeetID: 

## 项目清单

项目名称：JavaPackage

repository 地址：

- https://gitee.com/openeuler/java-package