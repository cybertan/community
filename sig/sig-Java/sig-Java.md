# openEuler Java Special Interest Group (SIG)
English | [简体中文](./sig-Java_cn.md)

The openEuler Java SIG aims at providing Java package solution and guideline to openEuler community, in order to reduce the threshold for using openEuler and promote openEuler to new users, Java applications and Java components.

## SIG Mission and Scope

### Mission
- 

### Scope
- 


### Repositories and description managed by this SIG

- Repository of scripts and docs for Java package: https://gitee.com/openeuler/java-package

## Basic Information

### Project Introduction
    https://gitee.com/openeuler/community/tree/master/sig/sig-Java/

### Maintainers
- luo-haibo

### Committers
- 
- 

### Mailing list
- dev@openeuler.org

### Slack Workspace
- 

### Meeting
- Time: 
- Zoom MeetID: 

### IRC Channel
- 

### External Contact
- 
